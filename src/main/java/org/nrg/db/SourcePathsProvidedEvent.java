package org.nrg.db;

import org.springframework.context.ApplicationEvent;

import java.nio.file.Path;
import java.util.List;

/**
 * Created by drm on 7/4/16.
 */
public class SourcePathsProvidedEvent extends ApplicationEvent {
    List<Path> paths;

    public SourcePathsProvidedEvent(Object source, List<Path> srcPaths) {
        super(source);
        this.paths = srcPaths;
        if( srcPaths.size() > 0) {
            System.out.println("Source paths provided:");
            for( Path p: paths) {
                System.out.println(p);
            }
        }
    }

    public List<Path> getPaths() {
        return paths;
    }
}
