package org.nrg.db;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.nrg.db.persistence.DicomBrowserRepository;
import org.nrg.db.persistence.JdbcDicomBrowserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * Created by drm on 7/2/16.
 */
@Configuration
@Import(ScreensConfiguration.class)
@ComponentScan
@EnableTransactionManagement
public class AppConfig {

    @Bean
    public DataSource datasource() throws PropertyVetoException {

//        ComboPooledDataSource cpds = new ComboPooledDataSource();
//        cpds.setDriverClass("org.postgresql.Driver"); //loads the jdbc driver
//        cpds.setJdbcUrl("jdbc:postgresql:dicombrowser");
//        cpds.setUser("sa");
//        cpds.setPassword("as");

        ComboPooledDataSource cpds = new ComboPooledDataSource();
        cpds.setDriverClass("org.h2.Driver"); //loads the jdbc driver
        cpds.setJdbcUrl("jdbc:h2:mem:dicomwrangler;DB_CLOSE_DELAY=-1");
        cpds.setUser("");
        cpds.setPassword("");

        cpds.setMinPoolSize(3);
        cpds.setAcquireIncrement(5);
        cpds.setMaxPoolSize(15);

        cpds.setMaxStatements(100);
        cpds.setMaxStatementsPerConnection(20);

        return cpds;
    }

    @Bean
    public PlatformTransactionManager txManager() throws PropertyVetoException {
        return new DataSourceTransactionManager(datasource());
    }

    @Bean
    public JdbcTemplate jdbcTemplate( DataSource dataSource) {
        return new JdbcTemplate( dataSource);
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate( dataSource);
    }

    public DicomBrowserRepository dicomBrowserRepository() {
        return new JdbcDicomBrowserRepository();
    }
}
