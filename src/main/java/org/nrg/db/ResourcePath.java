package org.nrg.db;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by drm on 6/24/16.
 */
public class ResourcePath {

    private Path path;
    private Path basePath;

    public ResourcePath( Path basePath, Path relativePath) {
        this.basePath = basePath;
        this.path = basePath.resolve(relativePath);
    }

    public Path getBasePath() {
        return basePath;
    }

    public Path getPath() {
        return path;
    }

    public Path getRelativePath() {
        return basePath.relativize(path);
    }

    public boolean isDirectory() {
        return ((path == null) || !Files.exists(path)) ? false : Files.isDirectory(path);
    }

//    public List<ResourcePath> listResources() {
//
//    }


    public static void main(String[] args) {
        Path basePath = Paths.get("/home/drm/foo");
        Path relativePath = Paths.get("/bar/fubar");

        ResourcePath rp = new ResourcePath( basePath, relativePath);

        System.out.format("BasePath = %s, RelativePath = %s\n", basePath.toString(), relativePath.toString());
        System.out.format("BasePath = %s, RelativePath = %s, Path = %s\n", rp.getBasePath().toString(), rp.getRelativePath().toString(),rp.getPath().toString());
    }
}
