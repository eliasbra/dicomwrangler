package org.nrg.db;

import javafx.scene.control.TreeItem;
import org.dcm4che2.data.Tag;
import org.nrg.db.model.FileResource;
import org.nrg.db.ui.*;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Create various tree views of the opened files.
 *
 */
public class TreeViewFactory {

    public static TreeItem<FileResourceTreeItem> createDefaultTreeViewRoot(List<FileResource> resources) {
        return createCustomTreeViewRoot( resources);
//        return createBetterDefaultTreeViewRoot( resources);
    }

    public static TreeItem<FileResourceTreeItem> createBetterDefaultTreeViewRoot(List<FileResource> resources) {
        return new BetterDefaultTreeView( resources).getRootItem();
    }

    public static TreeItem<FileResourceTreeItem> createCustomTreeViewRoot(List<FileResource> resources) {

        List<TreeItemRule> rules = new ArrayList<>();
        rules.add( TreeItemRuleFactory.createDicomFileRule( "Patient {0}", new int[]{0x00100010}));
        rules.add( TreeItemRuleFactory.createDicomFileRule( "Study", new int[]{0x00100010}));
        rules.add( TreeItemRuleFactory.createDicomFileRule( "{0} Series {1}", new int[]{Tag.Modality}, new int[]{Tag.SeriesNumber}));
        rules.add( TreeItemRuleFactory.createDicomFileRule( "Image {0}", new int[]{Tag.InstanceNumber}));

        return new CustomTreeView( resources, rules).getRootItem();
    }

}
