package org.nrg.db.model;

import org.dcm4che2.data.*;
import org.dcm4che2.util.TagUtils;

/**
 * Created by drm on 10/7/16.
 */
public class Tags {
    private static final org.dcm4che2.data.DicomObject dicomObject = new BasicDicomObject();

    public static String getName( int tag) {
        return dicomObject.nameOf( tag);
    }
}
