package org.nrg.db.model;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by drm on 10/24/16.
 */
public class FileResource {
    private int id;
    private Path basePath;
    private Path relativePath;

    public Path getFullPath() {
        return (basePath.resolve(relativePath));
    }

    public FileResource(int id, String basePath, String relativePath) {
        this.id = id;
        this.basePath = Paths.get(basePath);
        this.relativePath = Paths.get(relativePath);
    }

    public FileResource(int id, Path basePath, Path relativePath) {
        this.id = id;
        this.basePath = basePath;
        this.relativePath = relativePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Path getBasePath() {
        return basePath;
    }

    public void setBasePath(Path basePath) {
        this.basePath = basePath;
    }

    public Path getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(Path relativePath) {
        this.relativePath = relativePath;
    }

    public String getFileName() {
        return relativePath.getFileName().toString();
    }

}
