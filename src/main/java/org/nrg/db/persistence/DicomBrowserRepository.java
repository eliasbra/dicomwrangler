package org.nrg.db.persistence;

import org.nrg.db.model.DicomElement;
import org.nrg.db.model.FileResource;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created by drm on 6/17/16.
 */
//@Component
public interface DicomBrowserRepository {

    void createDB();
    void addFile(Path basePath, Path relativePath);
    void addFiles( Path basePath, List<Path> relativePaths);
    List<FileResource> getResources();
    int addAttribute( Integer fileId, String tagPath, String name, String value);
    List<DicomElement> getElements();

    DicomElement getElement(int fileId, int[] tagpath);

    List<DicomElement> getElements(List<Integer> selectedFileIds);
    List<DicomElement> getElements( List<Integer> selectedFileIds, String tagpath);
    void clear();
}
