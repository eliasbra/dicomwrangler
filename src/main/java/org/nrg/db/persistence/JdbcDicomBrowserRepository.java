package org.nrg.db.persistence;

import org.nrg.db.model.DicomObject;
import org.nrg.db.model.DicomElement;
import org.nrg.db.model.FileResource;
import org.nrg.db.persistence.DicomBrowserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

//import javax.inject.Inject;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by drm on 7/2/16.
 */
@Component
public class JdbcDicomBrowserRepository implements DicomBrowserRepository {

//    @Inject
    @Autowired
    private JdbcTemplate ops;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private SimpleJdbcInsert jdbcInsertFile;
    private SimpleJdbcInsert jdbcInsertAttributes;

    public JdbcDicomBrowserRepository() {
    }

    @PostConstruct
    public void init() {
        // comment the following line if using the on-disk DB and you don't want the previous sessioin to go away.
        createDB();
        jdbcInsertFile = new SimpleJdbcInsert(dataSource).withTableName("files").usingGeneratedKeyColumns("id");
        jdbcInsertAttributes = new SimpleJdbcInsert(dataSource).withTableName("attributes").usingGeneratedKeyColumns("id");
    }

    private static final String CREATE_FILE_TABLE =
            "create table files (\n" +
            "  id  SERIAL PRIMARY KEY,\n" +
            "  basepath varchar(1024),\n" +
            "  relativepath VARCHAR(1024),\n" +
            "  hash varchar(256),\n" +
            "  UNIQUE( basepath, relativepath)\n" +
            ")";

    private static final String CREATE_ATTRIBUTE_TABLE =
            "create table attributes (\n" +
            "  id SERIAL PRIMARY KEY,\n" +
            "  tagpath varchar(256),\n" +
            "  name varchar(256),\n" +
            "  value varchar(1024),\n" +
            "  UNIQUE( tagpath, value)\n" +
            ")";

    private static final String CREATE_FILE_ATTRIBUTE_TABLE =
            "create table files_attributes (\n" +
            "  id SERIAL PRIMARY KEY,\n" +
            "  file_id INTEGER REFERENCES files(id),\n" +
            "  attribute_id INTEGER REFERENCES attributes(id)\n" +
            ");\n";

    private static final String CREATE_INDICES = "create index attributes_index on attributes (tagpath, value);";

    @Override
    @Transactional
    public void createDB() {
        ops.execute( CREATE_FILE_TABLE);
        ops.execute( CREATE_ATTRIBUTE_TABLE);
        ops.execute( CREATE_FILE_ATTRIBUTE_TABLE);
        ops.execute( CREATE_INDICES);
    }

    private static String INSERT_FILE = "insert into files (basepath, relativepath) values (?, ?)";

    // "is not distinct from" is needed so that null = null matches as true.
    // private static String SELECT_ATTRIBUTE_BY_PARAMS = "select id from attributes where tagpath = ? and value = ?";
    private static String SELECT_ATTRIBUTE_BY_PARAMS = "select id from attributes where tagpath = ? and value is not distinct from ?";
    private static String SELECT_ATTRIBUTES = "select id, tagpath, name, value from attributes order by length(tagpath),tagpath";

    /**
     * Add the file at the specified location to the database.
     *
     * @param basePath
     * @param relativePath
     * @throws DuplicateKeyException if the file is already in DB.
     */
    @Override
    @Transactional
    public void addFile(Path basePath, Path relativePath) {
        try {
            DicomObject dicomObject = new DicomObject(basePath.resolve(relativePath).toFile());
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("basepath", basePath.toString())
                    .addValue("relativepath", relativePath.toString());
            int fileId = jdbcInsertFile.executeAndReturnKey(parameters).intValue();
            addAttributes(dicomObject, fileId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addFiles(Path basePath, List<Path> relativePaths) {
        for( Path p: relativePaths) {
            addFile( basePath, p);
        }
    }

    private static String SELECT_RESOURCES = "select id, basepath, relativepath from files";

    @Override
    public List<FileResource> getResources() {
        List<FileResource> resources = new ArrayList<>();
        ops.query(SELECT_RESOURCES, new Object[]{}, new RowMapper<List<FileResource>>() {
            @Override
            public List<FileResource> mapRow(ResultSet resultSet, int i) throws SQLException {
                int id = resultSet.getInt("id");
                String basePathString = resultSet.getString("basepath");
                String relativePathString = resultSet.getString("relativepath");
                FileResource resource = new FileResource( id, basePathString, relativePathString);
                resources.add(resource);
                return resources;
            }
        });
        return resources;
    }

    public void addAttributes(DicomObject dicomObject, int fileID) {
        Date starttime = new Date();
System.out.println("Start add attributes." + starttime);
        for(DicomElement dicomElement: dicomObject.getElements()) {
            int attributeID = addAttribute( fileID, dicomElement.getTagPathString(), dicomElement.getName(), dicomElement.getValue());
        }
        Date stoptime = new Date();
        System.out.println("Stop add attributes." + stoptime);
        System.out.println("Elapsed time: " + (stoptime.getTime() - starttime.getTime())/ 1000);
    }

    /**
     * Return the id of attribute, create it if it doesn't exist.
     *
     * @param tagPath
     * @param value
     * @return integer id of the attribute.
     */
//    @Override
//    public int addAttribute( String tagPath, String name, String value) {
//        int attribute_id;
//        System.out.println("Add attribute: " + tagPath);
//        try {
//            attribute_id = ops.queryForObject(SELECT_ATTRIBUTE_BY_PARAMS, Integer.class, tagPath, value);
//        }
//        catch( EmptyResultDataAccessException e) {
//            String v = value;
//            if( value != null && value.length() > 1024) {
//                v = value.substring(0,1024);
//            }
//            SqlParameterSource parameters = new MapSqlParameterSource()
//                    .addValue("tagpath", tagPath)
//                    .addValue("name", name)
//                    .addValue("value", v);
//            Number newId = jdbcInsertAttributes.executeAndReturnKey(parameters);
//            attribute_id = newId.intValue();
//        }
//
//        return attribute_id;
//    }

    @Override
    public int addAttribute( Integer fileId, String tagPath, String name, String value) {
        Integer attribute_id;
        System.out.println("Add attribute: " + tagPath);
        attribute_id = ops.query("select id from attributes where tagpath = ? and value = ?", new Object[]{tagPath, value}, new ResultSetExtractor<Integer>() {
            @Override
            public Integer extractData(ResultSet resultSet) throws SQLException {
                Integer id = null;
                if( resultSet.next()) {
                    id = resultSet.getInt("id");
                }
                return id;
            }
        });

        if( attribute_id == null) {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("tagpath", tagPath)
                    .addValue("name", name)
                    .addValue("value", (value != null && value.length() > 1024) ?  value.substring(0,1024): value);
            attribute_id = jdbcInsertAttributes.executeAndReturnKey(parameters).intValue();
        }
        ops.update( "insert into files_attributes (file_id, attribute_id) values (?, ?)", fileId, attribute_id);
        return attribute_id;
    }

//    @Override
//    public int addAttribute( Integer fileId, String tagPath, String name, String value) {
//        Integer attribute_id = null;
//        System.out.println("Add attribute: " + tagPath);
//
//        try {
//            SqlParameterSource parameters = new MapSqlParameterSource()
//                    .addValue("tagpath", tagPath)
//                    .addValue("name", name)
//                    .addValue("value", (value != null && value.length() > 1024) ?  value.substring(0,1024): value);
//            attribute_id = jdbcInsertAttributes.executeAndReturnKey(parameters).intValue();
//        }
//        catch( DuplicateKeyException e) {
//            attribute_id = ops.query("select id from attributes where tagpath = ? and value is not distinct from ?", new Object[]{tagPath, value}, new ResultSetExtractor<Integer>() {
//                @Override
//                public Integer extractData(ResultSet resultSet) throws SQLException {
//                    Integer id = null;
//                    if( resultSet.next()) {
//                        id = resultSet.getInt("id");
//                    }
//                    return id;
//                }
//            });
//        }
//        Integer a = attribute_id;
//        ops.update( "insert into files_attributes (file_id, attribute_id) values (?, ?)", fileId, attribute_id);
//        return attribute_id;
//    }

    @Override
    public List<DicomElement> getElements() {
        List<DicomElement> elements = new ArrayList<>();
        ops.query(SELECT_ATTRIBUTES, new Object[]{}, new RowMapper<List<DicomElement>>() {
            @Override
            public List<DicomElement> mapRow(ResultSet resultSet, int i) throws SQLException {
                String tagPathString = resultSet.getString("tagpath");
                String name = resultSet.getString("name");
                String value = resultSet.getString("value");
                DicomElement element = new DicomElement( tagPathString, name, value);
                elements.add(element);
                return elements;
            }
        });
        return elements;
    }

    private static String SELECT_ATTRIBUTE = "select tagpath, name, value from attributes, files_attributes where " +
            " files_attributes.attribute_id = attributes.id and files_attributes.file_id = ? and attributes.tagpath = ?";

    @Override
    public DicomElement getElement(int fileId, int[] tagpath) {

        String tagPathString = DicomElement.tagArrayToTagPath(tagpath);

        DicomElement element = ops.queryForObject(SELECT_ATTRIBUTE, new Object[]{fileId, tagPathString}, new RowMapper<DicomElement>() {
            @Override
            public DicomElement mapRow(ResultSet resultSet, int i) throws SQLException {
                String tagPathString = resultSet.getString("tagpath");
                String name = resultSet.getString("name");
                String value = resultSet.getString("value");
                return  new DicomElement( tagPathString, name, value);
            }
        });
        return element;
    }

    private static final String SELECT_ELEMENTS_WITH_FILE_IDS =
            "select x.tagpath, min(x.value) as \"value\", x.name, count(x.value) as \"count\" from\n" +
                    "(select a.tagpath, a.value, a.name, count( a.id) from attributes a, files_attributes\n" +
                    "where a.id = files_attributes.attribute_id\n" +
                    "and files_attributes.file_id IN (:ids)\n" +
                    "group by a.tagpath, a.value, a.name) x\n" +
                    "group by x.tagpath, x.name\n" +
                    "order by length(x.tagpath),x.tagpath;";

    @Override
    public List<DicomElement> getElements( List<Integer> selectedFileIds) {
        List<DicomElement> elements = new ArrayList<>();
        if( selectedFileIds.size() > 0) {
            MapSqlParameterSource parameters = new MapSqlParameterSource().addValue("ids", selectedFileIds);

            namedParameterJdbcTemplate.query(SELECT_ELEMENTS_WITH_FILE_IDS, parameters, new DicomElementRowMapper(elements));
        }
        return elements;
    }

    private static final String SELECT_ELEMENTS_WITH_FILE_IDS_AND_TAGPATH =
            "select x.tagpath, min(x.value) as \"value\", x.name, count(x.value) as \"count\" from\n" +
                    "(select a.tagpath, a.value, a.name, count( a.id) from attributes a, files_attributes\n" +
                    "where a.id = files_attributes.attribute_id\n" +
                    "and files_attributes.file_id IN (:ids)\n" +
                    "and a.tagpath like (:tagpathexpression)\n" +
                    "group by a.tagpath, a.value, a.name) x\n" +
                    "group by x.tagpath, x.name\n" +
                    "order by length(x.tagpath),x.tagpath;";

    @Override
    public List<DicomElement> getElements( List<Integer> selectedFileIds, String tagpath) {
        List<DicomElement> elements = new ArrayList<>();
        MapSqlParameterSource parameters = new MapSqlParameterSource().addValue( "ids", selectedFileIds);
        parameters.addValue( "tagpathexpression", tagpath + ",%");

        namedParameterJdbcTemplate.query(SELECT_ELEMENTS_WITH_FILE_IDS_AND_TAGPATH, parameters, new DicomElementRowMapper( elements) );
        return elements;
    }

    private class DicomElementRowMapper implements RowMapper<List<DicomElement>> {
        List<DicomElement> elements;
        public DicomElementRowMapper( List<DicomElement> elements) {
            this.elements = elements;
        }

        @Override
        public List<DicomElement> mapRow(ResultSet resultSet, int i) throws SQLException {
            String tagPathString = resultSet.getString("tagpath");
            String name = resultSet.getString("name");
            String value = resultSet.getString("value");
            int count = resultSet.getInt("count");
            DicomElement element = new DicomElement( tagPathString, name, value, count);
            elements.add(element);
            return elements;
        }
    }

    @Override
    public void clear() {
        ops.update("delete from files_attributes CASCADE ");
        ops.update("delete from attributes CASCADE ");
        ops.update("delete from files CASCADE ");
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
