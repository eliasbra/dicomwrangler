package org.nrg.db;

import javafx.scene.control.TreeItem;
import org.nrg.db.ui.DicomElementI;
import org.springframework.context.ApplicationEvent;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created by drm on 7/4/16.
 */
public class ChangeElementViewOrderEvent extends ApplicationEvent {
    TreeItem<DicomElementI> item;

    public ChangeElementViewOrderEvent(Object source, TreeItem<DicomElementI> item) {
        super(source);
        this.item = item;
    }

    public TreeItem<DicomElementI> getItem() {
        return item;
    }
}
