package org.nrg.db.ui;

import org.nrg.db.persistence.DicomBrowserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Create the rule objects that specify the content of the nodes in the file view.
 *
 * This mainly exists to inject the depencency on the persistence repository and hide this dependency from
 * the rest of the code.
 *
 * This is an alternative to making TreeItemRuleDicomFile which extends TreeItemRule<Integer> ApplicationContextAware.
 * This implementation simplifies that class but adds this one.
 *
 */
@Component
public class TreeItemRuleFactory {
    private static DicomBrowserRepository dbr;

    @Autowired
    public TreeItemRuleFactory( DicomBrowserRepository dbr) {
        this.dbr = dbr;
    }

    public static TreeItemRule<Integer> createDicomFileRule( String format, int[]... tagPaths) {
        return new TreeItemRuleDicomFile( dbr, format, Arrays.asList(tagPaths));
    }
}
