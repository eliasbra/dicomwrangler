package org.nrg.db.ui;

/**
 * Created by drm on 10/24/16.
 */
public class FileResourceTreeItem {
    private String displayName;
    private Integer fileId;

    public FileResourceTreeItem(String displayName, Integer fileId) {
        this.displayName = displayName;
        this.fileId = fileId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Integer getFileId() {
        return fileId;
    }

    public String toString() {
        return getDisplayName();
    }
}
