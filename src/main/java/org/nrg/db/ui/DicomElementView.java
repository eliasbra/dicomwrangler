package org.nrg.db.ui;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.util.Callback;
import org.nrg.db.ChangeElementViewOrderEvent;
import org.nrg.db.model.DicomElement;
import org.nrg.db.model.Tags;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import java.util.Arrays;
import java.util.List;

/**
 * Created by drm on 9/23/16.
 */
public class DicomElementView implements ApplicationEventPublisherAware {
    private TreeItem<DicomElementI> rootItem;

    private TreeTableColumn<DicomElementI, String> tagColumn;
    private TreeTableColumn<DicomElementI, String> nameColumn;
    private TreeTableColumn<DicomElementI, String> valueColumn;

    private ContextMenu tagContextMenu;
    private ContextMenu nameContextMenu;
    private ContextMenu valueContextMenu;

    private ApplicationEventPublisher publisher;

    public DicomElementView() {

        tagColumn = new TreeTableColumn<>("Tag");
        tagColumn.setPrefWidth(150);
        tagColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DicomElementI, String> param) ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getTagString())
        );
        tagColumn.setCellFactory(new Callback<TreeTableColumn<DicomElementI, String>, TreeTableCell<DicomElementI, String>>() {
            @Override
            public TreeTableCell<DicomElementI, String> call(TreeTableColumn<DicomElementI, String> param) {
                TreeTableCell<DicomElementI, String> ttc = new TextFieldTreeTableCell<DicomElementI, String>();
                ttc.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                    @Override
                    public void handle(ContextMenuEvent event) {
                        System.out.println("You asked for a context menu.");
                        System.out.println("Source: " + event.getSource());
                        tagContextMenu.show(ttc, Side.LEFT, 0, 0);
                    }
                });
                return ttc;
            }
        });
        nameColumn = new TreeTableColumn<>("Name");
        nameColumn.setPrefWidth(250);
        nameColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DicomElementI, String> param) ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getName())
        );
        nameColumn.setCellFactory(new Callback<TreeTableColumn<DicomElementI, String>, TreeTableCell<DicomElementI, String>>() {
            @Override
            public TreeTableCell<DicomElementI, String> call(TreeTableColumn<DicomElementI, String> param) {
                TreeTableCell<DicomElementI, String> ttc = new TextFieldTreeTableCell<DicomElementI, String>();
                ttc.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                    @Override
                    public void handle(ContextMenuEvent event) {
                        System.out.println("You asked for a context menu.");
                        System.out.println("Source: " + event.getSource());
                        nameContextMenu.show(ttc, Side.LEFT, 0, 0);
                    }
                });
                return ttc;
            }
        });
        valueColumn = new TreeTableColumn<>("Value");
        valueColumn.setPrefWidth(500);
        valueColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DicomElementI, String> param) ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getValue())
        );
        valueColumn.setCellFactory(new Callback<TreeTableColumn<DicomElementI, String>, TreeTableCell<DicomElementI, String>>() {

            @Override
            public TreeTableCell<DicomElementI, String> call(TreeTableColumn<DicomElementI, String> param) {
                TreeTableCell<DicomElementI, String> ttc = new TextFieldTreeTableCell<DicomElementI, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
//                        if( ! empty) {
                            TreeTableRow<DicomElementI> row = this.getTreeTableRow();

                            // under some circumstance, javafx will call this method without the item existing.
                            // wait until it exists to mess with it.
                            if( row.getTreeItem() != null) {
                                int occuranceCount = row.getTreeItem().getValue().getOccuranceCount();
                                if (occuranceCount > 1) {
                                    this.getStyleClass().add("multiple-entry-cell");
                                } else {
                                    this.getStyleClass().remove("multiple-entry-cell");
                                }
                            }
//                        }
                    }
                };
                ttc.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                    @Override
                    public void handle(ContextMenuEvent event) {
                        System.out.println("You asked for a context menu.");
                        System.out.println("Source: " + event.getSource());
                        valueContextMenu.show(ttc, Side.LEFT, 0, 0);
                    }
                });
                return ttc;
            }
        });

        tagContextMenu = new ContextMenu();
        MenuItem tagCopyMenuItem = new MenuItem("Copy");
        tagCopyMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Yeah, well, there's this.
                String s = valueColumn.treeTableViewProperty().getValue().getSelectionModel().getSelectedItem().getValue().getTagString();
                copyToClipboard( s);
            }
        });
        tagContextMenu.getItems().add( tagCopyMenuItem);
        MenuItem tagOrderMenuItem = new MenuItem("Order by tag");
        tagOrderMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                TreeItem<DicomElementI> selectedItem = tagColumn.getTreeTableView().getSelectionModel().getSelectedItem();
                publisher.publishEvent( new ChangeElementViewOrderEvent( this, selectedItem));
            }
        });
        tagContextMenu.getItems().add( tagOrderMenuItem);

        nameContextMenu = new ContextMenu();
        MenuItem nameCopyMenuItem = new MenuItem("Copy");
        nameCopyMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Yeah, well, there's this.
                String s = valueColumn.treeTableViewProperty().getValue().getSelectionModel().getSelectedItem().getValue().getName();
                copyToClipboard( s);
            }
        });
        nameContextMenu.getItems().add( nameCopyMenuItem);

        valueContextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem("Copy");
        copyMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Yeah, well, there's this.
                String s = valueColumn.treeTableViewProperty().getValue().getSelectionModel().getSelectedItem().getValue().getValue();
                copyToClipboard( s);
            }
        });
        valueContextMenu.getItems().add( copyMenuItem);
    }

    private void copyToClipboard( String s) {
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putString(s);
        clipboard.setContent(content);
    }

    public void setDicomElements(List<org.nrg.db.model.DicomElement> dicomElements) {
        rootItem = buildTree( dicomElements);
    }

    public List<TreeTableColumn<DicomElementI, String>> getColumns() {
        return Arrays.asList( tagColumn, nameColumn, valueColumn);
    }

    public TreeItem<DicomElementI> getRootItem() {
        return rootItem;
    }

    private TreeItem<DicomElementI> buildTree( List<DicomElement> dicomElements) {
        TreeItem<DicomElementI> root = new TreeItem<>( new DicomElementI());
        root.setExpanded(true);

        for( DicomElement de: dicomElements) {
            TreeItem<DicomElementI> parentItem = root;
            int[] tags = de.getTags();
            for( int i = 0; i < tags.length; i++) {
                parentItem = getItem( parentItem, i, tags, de.getValue(), de.getOccuranceCount());
            }
        }

        return root;
    }

    private TreeItem<DicomElementI> getItem( TreeItem<DicomElementI> parentItem, int index, int[] tags, String value, int count) {
        TreeItem<DicomElementI> item = findItem( parentItem, tags[index]);
        if( item == null) {
            item = newItem( parentItem, index, tags, value, count);
            parentItem.getChildren().add(item);
        }
        return item;
    }

    private TreeItem<DicomElementI> findItem( TreeItem<DicomElementI> parentItem, int tag) {
        TreeItem<DicomElementI> item = null;
        for( TreeItem<DicomElementI> el: parentItem.getChildren()) {
            if( el.getValue().getTag() == tag) {
                item = el;
                break;
            }
        }
        return item;
    }

    // TODO:  we could get the name from the DB, or not store the name in DB at all.
    private TreeItem<DicomElementI> newItem( TreeItem<DicomElementI> parentItem, int index, int[] tags, String value, int count) {
        DicomElementI element = null;
        int tag = tags[index];
        if( index % 2 == 0) {  // even
            if( index == tags.length-1) {
                element = new DicomElementI( tag, Tags.getName(tag), value, count);
            }
            else {
                element = new DicomElementI( tag, Tags.getName(tag), "", count);
            }
        }
        else {  // odd
            element = new DicomElementI( tag);
        }
        return new TreeItem<>( element);
    }

    public static void dumpElements( ObservableList<TreeItem<DicomElementI>> items, int indentLevel) {
        for( TreeItem<DicomElementI> item: items) {
            DicomElementI element = item.getValue();
            StringBuilder sb = new StringBuilder();
            for( int i = 0; i < indentLevel; i++) {
                sb.append("   ");
            }
            sb.append( element.getTagString() );
            sb.append( "," );
            sb.append( element.getName() );
            sb.append( "," );
            sb.append( element.getValue());
            System.out.println(sb.toString());
            if( item.isExpanded()) {
                dumpElements( item.getChildren(), indentLevel+1);
            }
        }
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

}
