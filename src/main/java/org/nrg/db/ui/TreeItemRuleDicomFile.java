package org.nrg.db.ui;


import org.nrg.db.model.DicomElement;
import org.nrg.db.model.FileResource;
import org.nrg.db.persistence.DicomBrowserRepository;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;


/**
 * Manages the content of TreeItems for the File TreeView.
 *
 * The context is an integer that is the fileId of the file in the DB.
 *
 * These objects are instantiated by the TreeItemRuleFactory.
 *
 */
public class TreeItemRuleDicomFile extends TreeItemRule< Integer> {
    private final List<int[]> tagPaths;
    private final String format;

    private DicomBrowserRepository dbr;

    protected TreeItemRuleDicomFile( DicomBrowserRepository dbr) {
        this( dbr, "default", Arrays.asList( new int[]{0x00100010}));
    }

    protected TreeItemRuleDicomFile( DicomBrowserRepository dbr, String format, List<int[]> tagPaths) {
        this.dbr = dbr;
        this.tagPaths = tagPaths;
        this.format = format;
    }

    protected String[] mapValues( Integer fileId, List<int[]> tagPaths) {
        String[] values = new String[tagPaths.size()];
        for( int i = 0; i < tagPaths.size(); i++) {
            DicomElement de = dbr.getElement(fileId, tagPaths.get(i));
            values[i] = (de != null)? de.getValue(): "null";
        }
        return values;
    }

    @Override
    protected String evaluate() {
        return MessageFormat.format( format, mapValues( context, tagPaths));
    }
}
