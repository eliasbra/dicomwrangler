package org.nrg.db.ui;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * The UI class representing DICOM Elements.
 * Created by drm on 9/27/16.
 */
public class DicomElementI {

    protected SimpleStringProperty tagString;
    protected SimpleStringProperty name;
    protected SimpleStringProperty value;
    protected SimpleIntegerProperty occuranceCount;
    protected int tag;
    protected boolean isItem;
    protected boolean isRoot;

    /**
     * Construct a "normal" or typical Dicom element for view.
     *
     * @param tag
     * @param name
     * @param value
     */
    public DicomElementI( int tag, String name, String value, int occuranceCount) {
        String s = String.format("%08X", tag);
        s = s.substring(0,4) + "," + s.substring(4,8);
        this.tagString = new SimpleStringProperty(this, "tagString", s);
        this.name = new SimpleStringProperty(this, "name", name);
        this.value = new SimpleStringProperty(this, "value", value);
        this.occuranceCount = new SimpleIntegerProperty(this, "count", occuranceCount);
        this.tag = tag;
        this.isItem = false;
        this.isRoot = false;
    }

    /**
     * Construct an Item DicomElement, representing the start of a new item in a sequence.
     *
     * @param itemNumber
     */
    public DicomElementI(int itemNumber) {
        tagString = new SimpleStringProperty(this, "tagString", "Item " + itemNumber);
        name = new SimpleStringProperty(this, "name",  "Item delimiter");
        value = new SimpleStringProperty(this, "value", "");
        occuranceCount = new SimpleIntegerProperty(this, "count", 1);
        this.tag = itemNumber;
        this.isItem = true;
        this.isRoot = false;
    }

    /**
     * Construct an Item DicomElement, representing the start of a sequence when ordered by tag.
     *
     * @param name
     */
    public DicomElementI(String name) {
        tagString = new SimpleStringProperty(this, "tagString", "0");
        this.name = new SimpleStringProperty(this, "name", name);
        value = new SimpleStringProperty(this, "value", "");
        occuranceCount = new SimpleIntegerProperty(this, "count", 1);
        this.tag = 0;
        this.isItem = true;
        this.isRoot = false;
    }

    /**
     * Construct an empty DicomElement, to be used as the root of the view tree.
     *
     */
    public DicomElementI() {
        tagString = new SimpleStringProperty(this, "tagString", "Root");
        name = new SimpleStringProperty(this, "name", "Root");
        value = new SimpleStringProperty(this, "value", "Root");
        occuranceCount = new SimpleIntegerProperty(this, "count", 1);
        this.tag = 0;
        this.isItem = false;
        this.isRoot = true;
    }

    public SimpleStringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name");
        }
        return name;
    }

    public SimpleStringProperty tagStringProperty() {
        if (tagString == null) {
            tagString = new SimpleStringProperty(this, "tagString");
        }
        return tagString;
    }

    public SimpleStringProperty valueProperty() {
        if (value == null) {
            value = new SimpleStringProperty(this, "value");
        }
        return value;
    }

    public SimpleIntegerProperty occuranceCountProperty() {
        if (occuranceCount == null) {
            occuranceCount = new SimpleIntegerProperty(this, "count");
        }
        return occuranceCount;
    }

    public String getName() {
        return name.getValue();
    }
    public void setName( String name) {
        this.name.setValue( name);
    }
    public String getTagString() {
        return tagString.getValue();
    }
    public void setTagString(String tag) {
        throw new UnsupportedOperationException();
    }
    public String getValue() {
        return value.getValue();
    }
    public void setValue( String value) {
        throw new UnsupportedOperationException();
    }
    public int getOccuranceCount() {
        return occuranceCount.getValue();
    }

    public void setTag( int tag) {
        this.tag = tag;
    }

    public int getTag() {
        return tag;
    }

    public boolean isItem() { return isItem;}
    public boolean isRoot() { return isRoot;}
}
