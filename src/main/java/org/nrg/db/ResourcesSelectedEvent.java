package org.nrg.db;

import org.springframework.context.ApplicationEvent;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created by drm on 7/4/16.
 */
public class ResourcesSelectedEvent extends ApplicationEvent {
    List<Integer> selectedFileIds;

    public ResourcesSelectedEvent(Object source, List<Integer> selectedFileIds) {
        super(source);
        this.selectedFileIds = selectedFileIds;
    }

    public List<Integer> getSelectedFileIds() {
        return selectedFileIds;
    }
}
