package org.nrg.db;

import org.nrg.db.persistence.DicomBrowserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.event.EventListener;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by drm on 7/4/16.
 */
@Component
public class HandleObjectPathsProvidedEvent implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher publisher;

    @Inject
    private DicomBrowserRepository dbr;

    @EventListener
    public void handleAddSourceFiles( ObjectPathsProvidedEvent event) {

        System.out.println("Handle the following source files:");
        System.out.println("===== " + dbr + " ======== " +this);
        Map<Path, List<Path>> map = event.getPaths();
        Map<Path, List<Path>> returnMap = new HashMap<>();
        for( Path basepath: map.keySet()) {
            List<Path> newPaths = new ArrayList<>();
            for( Path p: map.get(basepath)) {
                try {
                    System.out.println("store " + basepath + ", " + p);
                    dbr.addFile(basepath, p);
                    newPaths.add(p);

                } catch( DuplicateKeyException e) {
                    System.out.println( "skipping, all ready opened: " + basepath + ", " + p);
                }
            }
            returnMap.put( basepath, newPaths);
        }

        publisher.publishEvent( new ResourcesOpenedEvent( this, returnMap));
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

}
